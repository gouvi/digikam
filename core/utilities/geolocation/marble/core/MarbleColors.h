// SPDX-License-Identifier: LGPL-2.1-or-later
//
// SPDX-FileCopyrightText: 2012 Illya Kovalevskyy <illya.kovalevskyy@gmail.com>
//

// Oxygen color definitions from
// https://lists.kde.org/?l=kde-artists&m=116559781726568&q=p3

#ifndef MARBLECOLORS_H
#define MARBLECOLORS_H

#include "digikam_export.h"
#include <QColor>

namespace Marble
{
namespace Oxygen
{

DIGIKAM_EXPORT extern QColor const woodBrown6;
DIGIKAM_EXPORT extern QColor const woodBrown5;
DIGIKAM_EXPORT extern QColor const woodBrown4;
DIGIKAM_EXPORT extern QColor const woodBrown3;
DIGIKAM_EXPORT extern QColor const woodBrown2;
DIGIKAM_EXPORT extern QColor const woodBrown1;
DIGIKAM_EXPORT extern QColor const brickRed6;
DIGIKAM_EXPORT extern QColor const brickRed5;
DIGIKAM_EXPORT extern QColor const brickRed4;
DIGIKAM_EXPORT extern QColor const brickRed3;
DIGIKAM_EXPORT extern QColor const brickRed2;
DIGIKAM_EXPORT extern QColor const brickRed1;
DIGIKAM_EXPORT extern QColor const raspberryPink6;
DIGIKAM_EXPORT extern QColor const raspberryPink5;
DIGIKAM_EXPORT extern QColor const raspberryPink4;
DIGIKAM_EXPORT extern QColor const raspberryPink3;
DIGIKAM_EXPORT extern QColor const raspberryPink2;
DIGIKAM_EXPORT extern QColor const raspberryPink1;
DIGIKAM_EXPORT extern QColor const burgundyPurple6;
DIGIKAM_EXPORT extern QColor const burgundyPurple5;
DIGIKAM_EXPORT extern QColor const burgundyPurple4;
DIGIKAM_EXPORT extern QColor const burgundyPurple3;
DIGIKAM_EXPORT extern QColor const burgundyPurple2;
DIGIKAM_EXPORT extern QColor const burgundyPurple1;
DIGIKAM_EXPORT extern QColor const grapeViolet6;
DIGIKAM_EXPORT extern QColor const grapeViolet5;
DIGIKAM_EXPORT extern QColor const grapeViolet4;
DIGIKAM_EXPORT extern QColor const grapeViolet3;
DIGIKAM_EXPORT extern QColor const grapeViolet2;
DIGIKAM_EXPORT extern QColor const grapeViolet1;
DIGIKAM_EXPORT extern QColor const skyBlue6;
DIGIKAM_EXPORT extern QColor const skyBlue5;
DIGIKAM_EXPORT extern QColor const skyBlue4;
DIGIKAM_EXPORT extern QColor const skyBlue3;
DIGIKAM_EXPORT extern QColor const skyBlue2;
DIGIKAM_EXPORT extern QColor const skyBlue1;
DIGIKAM_EXPORT extern QColor const seaBlue6;
DIGIKAM_EXPORT extern QColor const seaBlue5;
DIGIKAM_EXPORT extern QColor const seaBlue4;
DIGIKAM_EXPORT extern QColor const seaBlue3;
DIGIKAM_EXPORT extern QColor const seaBlue2;
DIGIKAM_EXPORT extern QColor const seaBlue1;
DIGIKAM_EXPORT extern QColor const emeraldGreen6;
DIGIKAM_EXPORT extern QColor const emeraldGreen5;
DIGIKAM_EXPORT extern QColor const emeraldGreen4;
DIGIKAM_EXPORT extern QColor const emeraldGreen3;
DIGIKAM_EXPORT extern QColor const emeraldGreen2;
DIGIKAM_EXPORT extern QColor const emeraldGreen1;
DIGIKAM_EXPORT extern QColor const forestGreen6;
DIGIKAM_EXPORT extern QColor const forestGreen5;
DIGIKAM_EXPORT extern QColor const forestGreen4;
DIGIKAM_EXPORT extern QColor const forestGreen3;
DIGIKAM_EXPORT extern QColor const forestGreen2;
DIGIKAM_EXPORT extern QColor const forestGreen1;
DIGIKAM_EXPORT extern QColor const sunYellow6;
DIGIKAM_EXPORT extern QColor const sunYellow5;
DIGIKAM_EXPORT extern QColor const sunYellow4;
DIGIKAM_EXPORT extern QColor const sunYellow3;
DIGIKAM_EXPORT extern QColor const sunYellow2;
DIGIKAM_EXPORT extern QColor const sunYellow1;
DIGIKAM_EXPORT extern QColor const hotOrange6;
DIGIKAM_EXPORT extern QColor const hotOrange5;
DIGIKAM_EXPORT extern QColor const hotOrange4;
DIGIKAM_EXPORT extern QColor const hotOrange3;
DIGIKAM_EXPORT extern QColor const hotOrange2;
DIGIKAM_EXPORT extern QColor const hotOrange1;
DIGIKAM_EXPORT extern QColor const aluminumGray6;
DIGIKAM_EXPORT extern QColor const aluminumGray5;
DIGIKAM_EXPORT extern QColor const aluminumGray4;
DIGIKAM_EXPORT extern QColor const aluminumGray3;
DIGIKAM_EXPORT extern QColor const aluminumGray2;
DIGIKAM_EXPORT extern QColor const aluminumGray1;

}

}

#endif
